export class Noticia {
    titulo: string;
    corpo: string;
    img?: string;
    icone?: string = 'calendar';
    data: Date;
}

export class NoticiaServico {
    nome: string;
    noticias: Array<Noticia>;
}