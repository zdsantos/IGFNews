export class User {
    nome: string;
    sobrenome: string;
    email: string;
    nascimento: Date;
    foto: string;
    local: string;
    cidade: string;
    servicos: Array<string>;
}