import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp,  IonicModule,  IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { LoginPage } from "../pages/login/login";
import { ServicosPage } from "../pages/servicos/servicos";
import { PerfilPage } from '../pages/perfil/perfil';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { NoticiaPage } from '../pages/noticia/noticia';
import { CriarNoticiaPage } from '../pages/criar-noticia/criar-noticia';
import { MenuPopOver } from '../pages/menu-pop-over/menu-pop-over';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NoticiaProvider } from '../providers/noticia/noticia';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from "angularfire2/database";
import { UserProvider } from '../providers/user/user';


export const firebaseConfig = {
  apiKey: "AIzaSyBYNKUJmOoPoS5w0XgRlw5jflTtRbOgapc",
  authDomain: "igfnews-5b480.firebaseapp.com",
  databaseURL: "https://igfnews-5b480.firebaseio.com",
  projectId: "igfnews-5b480",
  storageBucket: "igfnews-5b480.appspot.com",
  messagingSenderId: "644776645913"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    PerfilPage,
    HomePage,
    TabsPage,
    MenuPopOver,
    CriarNoticiaPage,
    LoginPage,
    ServicosPage,
    NoticiaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    PerfilPage,
    HomePage,
    TabsPage,
    MenuPopOver,
    CriarNoticiaPage,
    LoginPage,
    ServicosPage,
    NoticiaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NoticiaProvider,
    UserProvider
  ]
})
export class AppModule {}
