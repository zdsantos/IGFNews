export class Utils {
    static dateToString(date: Date, separator?: string) {
        let d = date.getUTCDate(),
            m = date.getUTCMonth() + 1,
            y = date.getUTCFullYear();

        let D = d < 10 ? "0" + d : d;
        let M = m < 10 ? "0" + m : m;

        separator = separator ? separator : '/' ;

        return D + separator + M + separator + y;
    }
}