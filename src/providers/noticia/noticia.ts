import { NoticiaServico, Noticia } from '../../models/noticia';
import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";
import { Subscriber } from "rxjs/Subscriber";
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Injectable()
export class NoticiaProvider {

  constructor(private afDB: AngularFireDatabase) {
    console.log('Hello NoticiaProvider Provider');
  }

  getList() : FirebaseListObservable<Noticia[]> {
    return this.afDB.list("/noticias");
  }

  getNoticias(): Observable<Noticia[]> {
    return new Observable((subscriber: Subscriber<Noticia[]>) => {
      console.log('oi getNoticias');
      
      let result = [
        {
          titulo: "Inscrições para Coferência de Jovens em Sumaré",
          corpo: "Inscrições ocorreram até dia 14 de Junho. Interessados falar com a Leilane",
          icone: "calendar",
          data: new Date()
        },
        {
          titulo: "Comunhão de Casais",
          corpo: "Já temos data marcada! Será dias 17 a 20 de Agosto no Hotel Getsêmani. Inscrições com a Leilane.",
          img: "http://via.placeholder.com/250x200",
          icone: "heart",
          data: new Date()
        },
        {
          titulo: "Dia dos Namorados",
          corpo: "Teremos um lindo jantar no Local 3 às 19:30 para os casados. Venha celebrar o eterno amor juntamente com os irmãos na presença do Senhor. Todo o arrecadado será para ajudar os jovens a ir para conferência",
          img: "http://via.placeholder.com/250x200",
          icone: "heart",
          data: new Date()
        }
      ];

      subscriber.next(result);
    });
  }

  getNoticiasServicos(): Observable<NoticiaServico[]> {
    return new Observable((subscriber: Subscriber<NoticiaServico[]>) => {
      console.log('oi getNoticiasServicos');

      let result = [
        {
          nome: "Serviço de Música",
          noticias: [
            {
              titulo: "Dia dos Namorados",
              corpo: "Todos os jovens do serviço vão servir",
              icone: "musical-notes",
              data: new Date()
            },
            {
              titulo: "Ensaio Dia dos Namorados",
              corpo: "Todos os que vão serviço vão servir no Dia dos Namorados devem ir! Sexta às 19h no Local 1. Lista de musicas: Como eu te amo, É o teu amor maior que o eu, Lugar de Oração",
              icone: "information-circle",
              data: new Date()
            },
            {
              titulo: "Conferência com Ir. Ezra",
              corpo: "Obrigado pelo serviço na conferência. Ass: Ir. Ezra",
              icone: "calendar",
              data: new Date()
            },
            {
              titulo: "Escala de Junho",
              corpo: "Já está disponível",
              icone: "warning",
              data: new Date()
            }
          ]
        },
        {
          nome: "Serviço de Som",
          noticias: [
            {
              titulo: "Dia dos Namorados",
              corpo: "Todos os jovens do serviço vão servir",
              icone: "information-circle",
              data: new Date()
            },
            {
              titulo: "Escala de Junho",
              corpo: "Já está disponível",
              icone: "warning",
              data: new Date()
            }
          ]
        }
      ];

      subscriber.next(result);
    });
  }

}
