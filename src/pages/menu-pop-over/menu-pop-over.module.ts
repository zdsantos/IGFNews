import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuPopOver } from './menu-pop-over';

@NgModule({
  declarations: [
    MenuPopOver,
  ],
  imports: [
    IonicPageModule.forChild(MenuPopOver),
  ],
  exports: [
    MenuPopOver
  ]
})
export class MenuPopOverModule {}
