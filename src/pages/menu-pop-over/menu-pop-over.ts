import { Component } from '@angular/core';
import {ModalController, NavController,  NavParams} from 'ionic-angular';
import { CriarNoticiaPage } from "../criar-noticia/criar-noticia";

@Component({
  selector: 'page-menu-pop-over',
  templateUrl: 'menu-pop-over.html',
})
export class MenuPopOver {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPopOverPage');
  }

  pesquisar() {
    console.log("clicou no pesquisar");
  }

  criarNoticia() {
    this.modalCtrl.create(CriarNoticiaPage).present();
  }

}
