import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Noticia } from '../../models/noticia';
import { Utils } from "../../utils/utils";

@IonicPage()
@Component({
  selector: 'page-noticia',
  templateUrl: 'noticia.html',
})
export class NoticiaPage {

  noticia: Noticia;

  constructor(public navCtrl: NavController, public navParams: NavParams) {this.navParams.get('noticia')
    this.noticia = this.navParams.get('noticia');
    console.log(this.noticia);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoticiaPage');
  }

  dateToString(data: Date) {
    return Utils.dateToString(data);
  }

}
