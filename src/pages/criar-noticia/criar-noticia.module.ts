import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriarNoticiaPage } from './criar-noticia';

@NgModule({
  declarations: [
    CriarNoticiaPage,
  ],
  imports: [
    IonicPageModule.forChild(CriarNoticiaPage),
  ],
  exports: [
    CriarNoticiaPage
  ]
})
export class CriarNoticiaPageModule {}
