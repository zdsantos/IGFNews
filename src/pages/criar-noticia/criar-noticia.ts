import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-criar-noticia',
  templateUrl: 'criar-noticia.html',
})
export class CriarNoticiaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarNoticiaPage');
  }

  salvar() {
    console.log("clicou no salvar");
    
  }

}
