import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { User } from '../../models/user';

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html'
})
export class PerfilPage {

  user: User;

  constructor(public navCtrl: NavController, private userProvider: UserProvider) {
    // this.userProvider.getUser(1).subscribe((user) => {
    //   console.log(user);
    //   this.user = user[0];
    // });

    this.user = new User();
    this.user.nome = 'Fulano';
    this.user.sobrenome = 'de Tal';
    this.user.local = 'Local 2';
    this.user.cidade = 'Fortaleza';
    this.user.foto = 'http://via.placeholder.com/40x40';
  }

}
