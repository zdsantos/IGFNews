import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  credencial: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.credencial = { usuario: '' , senha: '' };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    console.log(this.credencial);
  }

  signInWithFacebook() {
    console.error('login com Facebook não implementado');
  }

  cadastrar() {
    this.navCtrl.setRoot(TabsPage);
  }

}
