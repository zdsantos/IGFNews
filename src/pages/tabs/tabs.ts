import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { PerfilPage } from '../perfil/perfil';
import { HomePage } from '../home/home';
import { ServicosPage } from '../servicos/servicos';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = PerfilPage;
  tab4Root = ServicosPage;

  constructor() {

  }
}
