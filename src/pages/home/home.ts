import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';
import { Noticia, NoticiaServico } from "../../models/noticia";
import { NoticiaProvider } from "../../providers/noticia/noticia";
import { MenuPopOver } from "../menu-pop-over/menu-pop-over";
import { FirebaseListObservable } from "angularfire2/database";
import { NoticiaPage } from '../noticia/noticia';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  eventos: any[];

  noticias: FirebaseListObservable<Noticia[]>;

  servicos: Array<NoticiaServico>;
  constructor(public navCtrl: NavController, public noticiaProvider: NoticiaProvider, public popoverCtrl: PopoverController) {
    this.eventos = [
      "http://via.placeholder.com/180x140",
      "http://via.placeholder.com/180x140",
      "http://via.placeholder.com/180x140",
      "http://via.placeholder.com/180x140",
      "http://via.placeholder.com/180x140"
    ];

  }

  ionViewWillLoad() {
    this.loadLists();
  }

  loadLists() {
    // this.noticiaProvider.getNoticias().subscribe((data) => {
    //   this.noticias = data;
    //   console.log(this.noticias);
    // });

    this.noticias = this.noticiaProvider.getList();
    this.noticiaProvider.getNoticiasServicos().subscribe((data) => {    
      this.servicos = data
      console.log(this.servicos);
    });
  }

  abrirNoticia(noticia) {
    this.navCtrl.push(NoticiaPage, {noticia: noticia});
  }

  presentMenu(event) {
    let menu = this.popoverCtrl.create(MenuPopOver);
    menu.present({
      ev: event
    });
  }

}